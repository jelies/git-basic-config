# This is my GIT config!
These are the set of files that I'm currently using to manage git in a easy way. Feel free to use them!

This repo includes:

- git aliases
- git basic config
- git-autocompletion script
- git diff using meld 3-way compare

Also includes some useful maven aliases (see `.bash_aliases` file).

## Scripts description

### apply-config.sh (copies the files from repo directory to home folder)
After doing a pull, just execute this script to update your local git files.

### update-project-files.sh (copies the files from home folder to repo)
Execute this script to update repository files from your local directory. 

## Requirements
Since I use meld as a diff tool, it needs to be installed to use it as a compare tool.


