#!/bin/bash
# This script updates the config located at home folder with the project files
name=$(git config --global user.name)
email=$(git config --global user.email)

## setting .gitconfig
rsync -v .gitconfig ~/.gitconfig
git config --global user.name $name
git config --global user.email $email

rsync -v .bash_aliases ~/.bash_aliases
rsync -v .bashrc ~/.bashrc
rsync -v git-meld ~/git-meld
rsync -v .git-completion.bash ~/.git-completion.bash

