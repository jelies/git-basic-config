#!/bin/bash
# This script updates the project files with the config located at home folder
name=$(git config --global user.name)
email=$(git config --global user.email)

## setting .gitconfig with default values
git config --global user.name "name"
git config --global user.email "email"
rsync -v ~/.gitconfig .gitconfig
git config --global user.name $name
git config --global user.email $email

rsync -v ~/.bash_aliases .bash_aliases
rsync -v ~/.bashrc .bashrc
rsync -v ~/git-meld git-meld

