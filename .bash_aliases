alias ll='ls -alF --color=tty'
alias mci='mvn clean install -DskipTests'
alias mcit='mvn clean install -Dit-test'
alias mcits='mvn clean install -Dit-test -DskipTests'
alias mcp='mvn clean package -DskipTests'
alias mcpt='mvn clean package -Dit-test'
alias mc='mvn install -DskipTests' #incremental compile
alias g='git'

